package com.miralak.basicaccelerometer.model;

import java.io.Serializable;

public class Orientation implements Serializable {

    private final float azimuth;
    private final float pitch;
    private final float roll;

    public Orientation(final float azimuth, final float pitch, final float roll) {
        this.azimuth = azimuth;
        this.pitch = pitch;
        this.roll = roll;
    }

    public float getRoll() {
        return roll;
    }

    public float getPitch() {
        return pitch;
    }

    public float getAzimuth() {
        return azimuth;
    }
}
