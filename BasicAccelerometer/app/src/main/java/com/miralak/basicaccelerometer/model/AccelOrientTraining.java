package com.miralak.basicaccelerometer.model;

public class AccelOrientTraining {
    private String userID;
    private long timestamp;
    private String activity;
    private long starttime;
    private Acceleration acceleration;
    private Orientation orientation;


    public void setStarttime(long starttime) {
        this.starttime = starttime;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setAcceleration(Acceleration acceleration) {
        this.acceleration = acceleration;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "AccelOrientTraining{" +
                "userID='" + userID + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", activity='" + activity + '\'' +
                ", starttime='" + starttime + '\'' +
                ", acceleration=" + acceleration + '\'' +
                ", orientation=" + orientation +
                '}';
    }
}
