package com.miralak.basicaccelerometer.model;

public class Gyro {

    private final float roll;
    private final float pitch;
    private final float yaw;

    public Gyro(float roll, float pitch, float yaw) {
        this.roll = roll;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public float getRoll() {
        return roll;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }
}
