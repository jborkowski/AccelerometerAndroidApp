package com.miralak.basicaccelerometer.model;

public class GyroProduction {
    private String userID;
    private long timestamp;
    private Gyro gyro;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Gyro getGyro() {
        return gyro;
    }

    public void setGyro(Gyro gyro) {
        this.gyro = gyro;
    }

    @Override
    public String toString() {
        return "AccelOrientProduction{" +
            "userID='" + userID + '\'' +
            ", timestamp='" + timestamp + '\'' +
            ", gyro=" + gyro +
            '}';
    }
}
