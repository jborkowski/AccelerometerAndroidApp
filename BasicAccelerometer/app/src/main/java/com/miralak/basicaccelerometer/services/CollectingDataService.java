package com.miralak.basicaccelerometer.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.miralak.basicaccelerometer.R;
import com.miralak.basicaccelerometer.activity.SettingActivity;
import com.miralak.basicaccelerometer.activity.StartActivity;
import com.miralak.basicaccelerometer.api.RestApi;
import com.miralak.basicaccelerometer.model.*;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CollectingDataService extends Service {

    private final String TAG = "CollectingDataService";
    private final long DELAY_BEFORE_RUN = 5000;
    public static int SERVICE_NOTIFICATION_ID = 1001131;

    private String restUri = "";
    private String userID = "";
    private String webSocketUri = "";
    private ActivityType activityType;
    private int acquisitionTime;
    private long startTime;
    private ServiceActions.Mode mode = ServiceActions.Mode.DEFAULT;

    private RestApi restApi;
    private CountDownTimer timer;
    private SensorManager sm;
    private Sensor accelerometer;
    private Sensor magnetometer;
    private Sensor gyrocope;

    private WebSocketClient mWebSocketClient;
    private NotificationManager mNotificationManager;

    private Handler handler;
    private final ToneGenerator toneGenerator;
    private AtomicBoolean isTimerStarted = new AtomicBoolean(false);
    private boolean isProductionMode = false;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (ServiceActions.STOP_ACTION.equals(intent.getAction())) {
                if (isTimerStarted.get()) {
                    timer.cancel();
                    timer.onFinish();
                    isTimerStarted.set(false);
                }
            }
        }
    };

    private final SensorEventListener accelOrientListener = new SensorEventListener() {
        float[] gravity;
        float[] geomagnetic;

        @Override
        public void onSensorChanged(final SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                gravity = sensorEvent.values;
            }

            if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                geomagnetic = sensorEvent.values;
            }

            if (gravity != null && geomagnetic != null) {
                float R[] = new float[9];
                float I[] = new float[9];
                final boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);
                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);
                    // orientation contains: azimut, pitch and roll
                    final Orientation capOrientation = new Orientation(orientation[0],
                                                                        orientation[1],
                                                                        orientation[2]);

                    final Acceleration capAcceleration = new Acceleration(geomagnetic[0],
                                                                        geomagnetic[1],
                                                                        geomagnetic[2]);


                    final Intent intent = new Intent(ServiceActions.APP_FEEDBACK_ACTION);
                    intent.putExtra(ServiceActions.EXTRA_ACCELERATION, capAcceleration);
                    sendBroadcast(intent);

                    sendAccelOrient(capAcceleration, capOrientation);
                }
            }
        }

        @Override
        public void onAccuracyChanged(final Sensor sensor, final int i) {
        }
    };

    private final SensorEventListener gyroListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(final SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                final Gyro gyro = new Gyro(sensorEvent.values[0],
                                            sensorEvent.values[1],
                                            sensorEvent.values[2]);
                sendGyro(gyro);
            }
        }

        @Override
        public void onAccuracyChanged(final Sensor sensor, final int i) {
        }
    };

    private Runnable runnableKeepAlive = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "WebSocket: Send keepalive");
            mWebSocketClient.send("keepalive");
            // Repeat this action again after 1 minute (while(1))
            handler.postDelayed(runnableKeepAlive, 60000);
        }
    };

    private final class WebSocket extends WebSocketClient {

        public WebSocket(URI serverURI) {
            super(serverURI);
        }

        @Override
        public void onOpen(final ServerHandshake handshakedata) {
            Log.i(TAG, "WebSocket: Opened");
            mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            handler.post(runnableKeepAlive);
        }

        @Override
        public void onMessage(final String message) {
            Log.d(TAG, "WebSocket: New Msg");
            final Notification notification = buildProductionNotification(message);
            mNotificationManager.notify(SERVICE_NOTIFICATION_ID, notification);
        }

        @Override
        public void onClose(final int code, final String reason, final boolean remote) {
            Log.i(TAG, "WebSocket: Closed " + reason);
            handler.removeCallbacks(runnableKeepAlive);
        }

        @Override
        public void onError(final Exception ex) {
            Log.e(TAG, "WebSocket: Error", ex);
            Log.i(TAG, "WebSocket: stop sending keepalive");
            handler.removeCallbacks(runnableKeepAlive);
        }
    }

    private final class CollectTimer extends CountDownTimer {

        public CollectTimer(final long millisInFuture, final long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(final long l) {
        }

        @Override
        public void onFinish() {
            toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD);
            stopGyroscope();
            stopAccelOrient();

            sendBroadcast(new Intent(ServiceActions.FINISH_ACTION));
            stopForeground(true);
        }
    }

    public CollectingDataService() {
        toneGenerator = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Init accelerometer sensor
        sm = (SensorManager) getApplication().getSystemService(SENSOR_SERVICE);
        gyrocope = sm.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        handler = new Handler(Looper.getMainLooper());

        final IntentFilter intentFilter = new IntentFilter(ServiceActions.STOP_ACTION);
        intentFilter.addAction(ServiceActions.SETTING_ACTION);
        intentFilter.addAction(ServiceActions.STOP_SERVICE_ACTION);
        intentFilter.addAction(ServiceActions.REFRESH_ACTION);
        registerReceiver(receiver, intentFilter);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        final String action = intent.getAction();

        if (ServiceActions.START_ACTION.equals(action)) {
            Log.i(TAG, "Received Start Foreground Intent ");
            restUri = intent.getStringExtra(ServiceActions.EXTRA_REST_URI);
            userID = intent.getStringExtra(ServiceActions.EXTRA_USER);
            mode = (ServiceActions.Mode) intent.getSerializableExtra(ServiceActions.EXTRA_MODE);

            if (intent.getBooleanExtra(ServiceActions.EXTRA_PRODUCTION_MODE, false)) {
                isProductionMode = true;
                activityType = ActivityType.JOGGING;
                webSocketUri = intent.getStringExtra(ServiceActions.EXTRA_WEB_SOCKET_URI);
            } else {
                acquisitionTime = intent.getIntExtra(ServiceActions.EXTRA_ACQUISITION_TIME, 20000);
                activityType = (ActivityType) intent.getSerializableExtra(ServiceActions.EXTRA_ACTIVITY_TYPE);
            }

            startTime = new Date().getTime();

            initRest();
            init();

            if (isProductionMode) {
                final Notification notification = buildProductionNotification(ActivityType.JOGGING.toString());
                connectWebSocket();
                startForeground(SERVICE_NOTIFICATION_ID, notification);
                //connectWebSocket();
            } else {
                Notification notification = new Notification();
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                    notification.priority = Notification.PRIORITY_MIN;
                }
                startForeground(R.string.app_name, notification);
            }
        } else if (ServiceActions.REFRESH_ACTION.equals(intent.getAction())) {

        } else if (ServiceActions.STOP_SERVICE_ACTION.equals(intent.getAction())) {
            timer.onFinish();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopGyroscope();
        stopAccelOrient();
        unregisterReceiver(receiver);
        handler.removeCallbacks(runnableKeepAlive);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initRest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(restUri)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        restApi = retrofit.create(RestApi.class);
    }

    private void connectWebSocket() {
        URI uri;
        try {
            uri = new URI(webSocketUri + "/" + userID);
        } catch (final URISyntaxException e) {
            Log.e (TAG, "Wrong Uri syntax: ", e);
            return;
        }

        mWebSocketClient = new WebSocket(uri);
    }

    private void init() {
        timer = new CollectTimer(acquisitionTime, acquisitionTime / 10);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Start sensors
                switch (mode) {
                    case GYRO:
                        startGyroscope(SensorManager.SENSOR_DELAY_GAME);
                        break;
                    case ACCELORIENT:
                        startAccelOrient(SensorManager.SENSOR_DELAY_GAME);
                        break;
                    case DEFAULT:
                        startAccelOrient(SensorManager.SENSOR_DELAY_GAME);
                        startGyroscope(SensorManager.SENSOR_DELAY_GAME);
                        break;
                }

                if (!isProductionMode) {
                    timer.start();
                    isTimerStarted.set(true);
                }

                toneGenerator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
            }
        }, DELAY_BEFORE_RUN);
    }

    private Notification buildProductionNotification(final String activity) {

        final Intent notificationIntent = new Intent(this, StartActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        final Intent refreshIntent = new Intent(this, CollectingDataService.class);
        refreshIntent.setAction(ServiceActions.REFRESH_ACTION);
        PendingIntent prefreshIntent = PendingIntent.getService(this, 0, refreshIntent, 0);

        final Intent settingIntent = new Intent(this, SettingActivity.class);
        settingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent psettingIntent = PendingIntent.getActivity(this, 0, settingIntent, 0);

        final Intent stopIntent = new Intent(this, CollectingDataService.class);
        stopIntent.setAction(ServiceActions.STOP_SERVICE_ACTION);
        PendingIntent pstopIntent = PendingIntent.getService(this, 0, stopIntent, PendingIntent.FLAG_ONE_SHOT);

        final Bitmap icon = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_dialog_dialer);

        final Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("Production Mode")
                .setContentText("Your activity is: " + activity)
                .setSmallIcon(android.R.drawable.ic_lock_lock)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .addAction(android.R.drawable.stat_notify_chat, "Refresh", prefreshIntent)
                .addAction(android.R.drawable.ic_menu_preferences, "Setting", psettingIntent)
                .addAction(android.R.drawable.star_on, "EXIT", pstopIntent)
                .build();
        return notification;
    }

    private void startGyroscope(final int sensorMode) {
        sm.registerListener(gyroListener, sm.getDefaultSensor(Sensor.TYPE_GYROSCOPE), sensorMode);
    }

    private void stopGyroscope() {
        sm.unregisterListener(gyroListener);
    }

    private void startAccelOrient(final int sensorMode) {
        sm.registerListener(accelOrientListener, accelerometer, sensorMode);
        sm.registerListener(accelOrientListener, sm.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), sensorMode);
    }

    private void stopAccelOrient() {
        sm.unregisterListener(accelOrientListener);
    }

    private void sendAccelOrient(final Acceleration acceleration, final Orientation orientation) {
        if (isProductionMode) {
            final AccelOrientProduction production = new AccelOrientProduction();
            production.setTimestamp(new Date().getTime());
            production.setUserID(userID);
            production.setOrientation(orientation);
            production.setAcceleration(acceleration);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    restApi.sendAccelOrientProduction(production)
                            .enqueue(new Callback<AccelOrientProduction>() {
                                @Override
                                public void onResponse(Call<AccelOrientProduction> call,
                                                       Response<AccelOrientProduction> response) {

                                }

                                @Override
                                public void onFailure(Call<AccelOrientProduction> call,
                                                      Throwable t) {

                                }
                            });
                }
            }).run();

        } else {
            final AccelOrientTraining training = new AccelOrientTraining();
            training.setStarttime(startTime);
            training.setTimestamp(new Date().getTime());
            training.setAcceleration(acceleration);
            training.setOrientation(orientation);
            training.setUserID(userID);
            training.setActivity(activityType.getLabel());

            new Thread(new Runnable() {
                @Override
                public void run() {
                    restApi.sendAccelOrientTraining(training)
                            .enqueue(new Callback<AccelOrientTraining>() {
                                @Override
                                public void onResponse(Call<AccelOrientTraining> call,
                                                       Response<AccelOrientTraining> response) {

                                }

                                @Override
                                public void onFailure(Call<AccelOrientTraining> call, Throwable t) {

                                }
                            });
                }
            }).run();
        }
    }

    private void sendGyro(final Gyro gyro) {
        if (isProductionMode) {
            final GyroProduction production = new GyroProduction();
            production.setTimestamp(new Date().getTime());
            production.setUserID(userID);
            production.setGyro(gyro);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    restApi.sendGyroProduction(production)
                            .enqueue(new Callback<GyroProduction>() {
                                @Override
                                public void onResponse(Call<GyroProduction> call,
                                                       Response<GyroProduction> response) {

                                }

                                @Override
                                public void onFailure(Call<GyroProduction> call, Throwable t) {

                                }
                            });
                }
            }).run();

        } else {
            final GyroTraining training = new GyroTraining();
            training.setTimestamp(new Date().getTime());
            training.setUserID(userID);
            training.setActivity(activityType.getLabel());
            training.setGyro(gyro);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    restApi.sendGyroTraining(training)
                            .enqueue(new Callback<GyroTraining>() {
                                @Override
                                public void onResponse(Call<GyroTraining> call,
                                                       Response<GyroTraining> response) {

                                }

                                @Override
                                public void onFailure(Call<GyroTraining> call, Throwable t) {

                                }
                            });
                }
            }).run();
        }
    }
}
