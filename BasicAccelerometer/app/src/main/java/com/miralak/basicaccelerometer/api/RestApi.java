package com.miralak.basicaccelerometer.api;

import com.miralak.basicaccelerometer.model.AccelOrientProduction;
import com.miralak.basicaccelerometer.model.AccelOrientTraining;
import com.miralak.basicaccelerometer.model.GyroProduction;
import com.miralak.basicaccelerometer.model.GyroTraining;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface RestApi {

    @POST("/production/accelorient")
    Call<AccelOrientProduction> sendAccelOrientProduction(@Body AccelOrientProduction accelOrientProduction);

    @POST("/training/accelorient")
    Call<AccelOrientTraining> sendAccelOrientTraining(@Body AccelOrientTraining accelOrientTraining);

    @POST("/production/gyro")
    Call<GyroProduction> sendGyroProduction(@Body GyroProduction gyroProduction);

    @POST("/training/gyro")
    Call<GyroTraining> sendGyroTraining(@Body GyroTraining gyroTraining);
}
