package com.miralak.basicaccelerometer.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.*;
import com.miralak.basicaccelerometer.R;
import com.miralak.basicaccelerometer.SettingsKey;
import com.miralak.basicaccelerometer.services.ServiceActions;

public class SettingActivity extends ActionBarActivity {
    private EditText restUri;
    private EditText webSocketUri;
    private EditText userID;
    private EditText acquisitionTime;
    private Button saveBtn;
    private Switch appMode;
    private Spinner acquisitionMode;
    private ServiceActions.Mode mode;

    private SharedPreferences prefs;

    private final CompoundButton.OnCheckedChangeListener appModeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            setSwitch(isChecked);
        }
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_layout);

        restUri = (EditText) findViewById(R.id.restUri);
        webSocketUri = (EditText) findViewById(R.id.webSocketUri);
        userID = (EditText) findViewById(R.id.userID);
        acquisitionTime = (EditText) findViewById(R.id.acquisitionTime);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        appMode = (Switch) findViewById(R.id.appMode);
        acquisitionMode = (Spinner) findViewById(R.id.acquisitionMode);

        saveBtn.setOnClickListener(onBtnClick);
        appMode.setOnCheckedChangeListener(appModeListener);

        prefs = getSharedPreferences(SettingsKey.appSetting, MODE_PRIVATE);
        loadPref();
    }

    private void loadPref() {
        acquisitionTime.setText(String.valueOf(prefs.getInt(SettingsKey.acquisitionTime, 20000)));
        userID.setText(prefs.getString(SettingsKey.userID, ""));
        restUri.setText(prefs.getString(SettingsKey.restUri, ""));
        webSocketUri.setText(prefs.getString(SettingsKey.webSockerUri, ""));
        setSwitch(prefs.getBoolean(SettingsKey.appMode, false));
        initActivitySpinner();

    }

    private final View.OnClickListener onBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            final SharedPreferences.Editor editor = prefs.edit();
            editor.putString(SettingsKey.userID, userID.getText().toString());
            editor.putString(SettingsKey.restUri, restUri.getText().toString());
            editor.putString(SettingsKey.webSockerUri, webSocketUri.getText().toString());
            editor.putInt(SettingsKey.acquisitionTime, Integer.parseInt(acquisitionTime.getText().toString()));
            editor.putBoolean(SettingsKey.appMode, appMode.isChecked());
            editor.putBoolean(SettingsKey.firstUse, false);
            editor.putString(SettingsKey.acquisitionMode, acquisitionMode.getSelectedItem().toString());

            editor.commit();

            finish();
        }
    };

    private void setSwitch(final boolean checked) {
        if (checked) {
            appMode.setText("Training mode");
            appMode.setChecked(true);
        } else {
            appMode.setText("Production mode");
            appMode.setChecked(false);
        }
    }

    private void initActivitySpinner() {
        final ArrayAdapter<ServiceActions.Mode> adapter = new ArrayAdapter<ServiceActions.Mode> (
                this,
                android.R.layout.simple_spinner_item,
                ServiceActions.Mode.values()
        );

        final ServiceActions.Mode actualMode =
                ServiceActions.Mode.valueOf(prefs.getString(SettingsKey.acquisitionMode, "DEFAULT"));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        acquisitionMode.setAdapter(adapter);
        acquisitionMode.setSelection(adapter.getPosition(actualMode));
    }

}
