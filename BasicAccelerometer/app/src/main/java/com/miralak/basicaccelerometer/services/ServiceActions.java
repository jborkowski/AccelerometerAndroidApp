package com.miralak.basicaccelerometer.services;

public interface ServiceActions {
    String START_ACTION = "com.miralak.basicaccelerometer.actions.START_ACTION";
    String STOP_ACTION = "com.miralak.basicaccelerometer.actions.STOP_ACTION";
    String EXTRA_ACQUISITION_TIME = "com.miralak.basicaccelerometer.extra.EXTRA_ACQUISITION_TIME";
    String EXTRA_REST_URI = "pl.tracking.androidapp.extra.EXTRA_REST_URI";
    String EXTRA_ACTIVITY_TYPE = "pl.tracking.androidapp.extra.EXTRA_ACTIVITY_TYPE";
    String EXTRA_MODE = "pl.tracking.androidapp.extra.EXTRA_MODE";
    String EXTRA_USER = "pl.tracking.androidapp.extra.USER";
    String EXTRA_WEB_SOCKET_URI = "pl.tracking.androidapp.extra.EXTRA_WEB_SOCKET_URI";
    String EXTRA_PRODUCTION_MODE = "pl.tracking.androidapp.extra.EXTRA_PRODUCTION_MODE";

    String FINISH_ACTION = "pl.tracking.androidapp.extra.FINISH_ACTION";

    String APP_FEEDBACK_ACTION = "pl.tracking.androidapp.extra.APP_FEEDBACK_ACTION";
    String EXTRA_ACCELERATION = "pl.tracking.androidapp.extra.EXTRA_ACCELERATION";
    String EXTRA_ORIENTATION = "pl.tracking.androidapp.extra.EXTRA_ORIENTATION";

    String REFRESH_ACTION = "pl.tracking.androidapp.actions.REFRESH_ACTION";
    String SETTING_ACTION = "pl.tracking.androidapp.actions.SETTING_ACTION";
    String STOP_SERVICE_ACTION = "pl.tracking.androidapp.actions.STOP_SERVICE_ACTION";


    enum Mode {
        DEFAULT,
        ACCELORIENT,
        GYRO
    }
}
