package com.miralak.basicaccelerometer.model;

public class GyroTraining {
    private String userID;
    private long timestamp;
    private String activity;
    private long starttime;
    private Gyro gyro;

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStarttime(long starttime) {
        this.starttime = starttime;
    }

    public void setGyro(Gyro gyro) {
        this.gyro = gyro;
    }

    @Override
    public String toString() {
        return "AccelOrientTraining{" +
            "userID='" + userID + '\'' +
            ", timestamp='" + timestamp + '\'' +
            ", activity='" + activity + '\'' +
            ", starttime='" + starttime + '\'' +
            ", gyro=" + gyro +
            '}';
    }
}
