package com.miralak.basicaccelerometer.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.miralak.basicaccelerometer.SettingsKey;
import com.miralak.basicaccelerometer.services.CollectingDataService;
import com.miralak.basicaccelerometer.services.ServiceActions;

public class StartActivity extends Activity {

    SharedPreferences prefs = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences(SettingsKey.appSetting, MODE_PRIVATE);

        if (prefs.getBoolean(SettingsKey.firstUse, true)) {
            final Intent intent = new Intent();
            intent.setClass(StartActivity.this, SettingActivity.class);
            startActivity(intent);
        } else if (prefs.getBoolean(SettingsKey.appMode, false)) {
            final Intent intent = new Intent();
            intent.setClass(StartActivity.this, CollectDataActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (!prefs.getBoolean(SettingsKey.appMode, false)) {
            //TODO: Production Implement this!!
            final Intent intent = new Intent(getBaseContext(), CollectingDataService.class);
            intent.setAction(ServiceActions.START_ACTION);
            intent.putExtra(ServiceActions.EXTRA_USER, prefs.getString(SettingsKey.userID, ""));
            intent.putExtra(ServiceActions.EXTRA_MODE, ServiceActions.Mode.DEFAULT);
            intent.putExtra(ServiceActions.EXTRA_REST_URI, prefs.getString(SettingsKey.restUri, ""));
            intent.putExtra(ServiceActions.EXTRA_WEB_SOCKET_URI, prefs.getString(SettingsKey.webSockerUri, ""));
            intent.putExtra(ServiceActions.EXTRA_PRODUCTION_MODE, true);
            startService(intent);
            finish();
        }
    }
}
