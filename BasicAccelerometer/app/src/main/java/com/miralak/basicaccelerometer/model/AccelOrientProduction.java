package com.miralak.basicaccelerometer.model;

public class AccelOrientProduction {
    private String userID;
    private long timestamp;
    private Acceleration acceleration;
    private Orientation orientation;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Acceleration getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(Acceleration acceleration) {
        this.acceleration = acceleration;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public String toString() {
        return "AccelOrientProduction{" +
            "userID='" + userID + '\'' +
            ", timestamp='" + timestamp + '\'' +
            ", acceleration=" + acceleration + '\'' +
            ", orientation=" + orientation +
            '}';
    }
}
