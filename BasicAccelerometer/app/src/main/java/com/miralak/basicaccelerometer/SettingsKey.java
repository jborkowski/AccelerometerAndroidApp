package com.miralak.basicaccelerometer;

public interface SettingsKey {
    String appSetting = "app_setting";
    String firstUse = "first_use";
    String userID = "user_id";
    String restUri = "rest_uri";
    String webSockerUri = "web_socket_uri";
    String acquisitionTime = "acquisition_time";
    String acquisitionMode = "acquisition_mode";
    String appMode = "application_mode";
}
