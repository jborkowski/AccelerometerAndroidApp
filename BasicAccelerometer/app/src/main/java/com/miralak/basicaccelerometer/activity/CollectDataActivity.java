package com.miralak.basicaccelerometer.activity;


import android.content.*;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.miralak.basicaccelerometer.R;
import com.miralak.basicaccelerometer.SettingsKey;
import com.miralak.basicaccelerometer.model.Acceleration;
import com.miralak.basicaccelerometer.model.ActivityType;
import com.miralak.basicaccelerometer.model.Orientation;
import com.miralak.basicaccelerometer.services.CollectingDataService;
import com.miralak.basicaccelerometer.services.ServiceActions;

public class CollectDataActivity extends ActionBarActivity {

    private TextView acceleration;
    private Spinner activitySpinner;
    private Button myStartButton;
    private Button myStopButton;

    private SharedPreferences prefs;
    private ActivityType selectedActivity = ActivityType.DOWNSTAIRS;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (ServiceActions.APP_FEEDBACK_ACTION.equals(intent.getAction())) {
                if (intent.hasExtra(ServiceActions.EXTRA_ACCELERATION)) {
                    final Acceleration acceleration =
                            (Acceleration) intent.getSerializableExtra(ServiceActions.EXTRA_ACCELERATION);
                    //updateTextView(acceleration);
                }
                if (intent.hasExtra(ServiceActions.EXTRA_ORIENTATION)) {
                    final Orientation orientation =
                        (Orientation) intent.getSerializableExtra(ServiceActions.EXTRA_ORIENTATION);
                    updateTextView(orientation);
                }
            } else if (ServiceActions.FINISH_ACTION.equals(intent.getAction())) {
                myStartButton.setVisibility(View.VISIBLE);
                myStopButton.setVisibility(View.GONE);
            }
        }
    };

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collect_data);
        acceleration = (TextView) findViewById(R.id.acceleration);

        prefs = getSharedPreferences(SettingsKey.appSetting, MODE_PRIVATE);

        initActivitySpinner();
        initActionButtons();

        final IntentFilter filter = new IntentFilter(ServiceActions.APP_FEEDBACK_ACTION);
        filter.addAction(ServiceActions.FINISH_ACTION);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accelerometer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            final Intent intent = new Intent();
            intent.setClass(CollectDataActivity.this, SettingActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initActionButtons() {
        myStartButton = (Button) findViewById(R.id.button_start_training);
        myStopButton = (Button) findViewById(R.id.button_stop_training);

        myStartButton.setVisibility(View.VISIBLE);
        myStopButton.setVisibility(View.GONE);

        myStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getBaseContext(), CollectingDataService.class);
                intent.setAction(ServiceActions.START_ACTION);
                intent.putExtra(ServiceActions.EXTRA_USER, prefs.getString(SettingsKey.userID, ""));
                intent.putExtra(ServiceActions.EXTRA_MODE,
                    ServiceActions.Mode.valueOf(prefs.getString(SettingsKey.acquisitionMode, "DEFAULT")));
                intent.putExtra(ServiceActions.EXTRA_ACTIVITY_TYPE, selectedActivity);
                intent.putExtra(ServiceActions.EXTRA_REST_URI, prefs.getString(SettingsKey.restUri, ""));
                intent.putExtra(ServiceActions.EXTRA_ACQUISITION_TIME, prefs.getInt(SettingsKey.acquisitionTime, 20000));
                startService(intent);

                myStartButton.setVisibility(View.GONE);
                myStopButton.setVisibility(View.VISIBLE);

            }
        });


        myStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent(ServiceActions.STOP_ACTION));
                myStartButton.setVisibility(View.VISIBLE);
                myStopButton.setVisibility(View.GONE);
            }
        });
    }

    private void initActivitySpinner() {
        activitySpinner = (Spinner) findViewById(R.id.spinner_activity);

        ArrayAdapter<ActivityType> adapter = new ArrayAdapter<ActivityType> (
                this,
                android.R.layout.simple_spinner_item,
                ActivityType.values()
        );

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activitySpinner.setAdapter(adapter);

        activitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                selectedActivity = (ActivityType) activitySpinner.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                selectedActivity = ActivityType.DOWNSTAIRS;
            }

        });
    }


    private void updateTextView(final Acceleration capturedAcceleration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                acceleration.setText("X:" + capturedAcceleration.getX() +
                        "\nY:" + capturedAcceleration.getY() +
                        "\nZ:" + capturedAcceleration.getZ());
            }
        });
    }

    private void updateTextView(final Orientation capturedOrientation) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                acceleration.setText("X:" + capturedOrientation.getPitch() +
                    "\nY:" + capturedOrientation.getAzimuth() +
                    "\nZ:" + capturedOrientation.getRoll() );
            }
        });
    }
}
