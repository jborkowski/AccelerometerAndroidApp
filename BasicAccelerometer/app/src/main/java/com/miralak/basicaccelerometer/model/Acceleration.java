package com.miralak.basicaccelerometer.model;

import java.io.Serializable;

public class Acceleration implements Serializable {

    private final float x;
    private final float y;
    private final float z;

    public Acceleration(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
}
